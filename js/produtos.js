const produtos = [
    { id: 1, name: "bolinha", valor: 100, descricao: "produto muito bom" },
    { id: 2, name: "bolão", valor: 200, descricao: "produto mais ou menos" },
    {
        id: 3,
        name: "bola quadrada",
        valor: 300,
        descricao: "produto sem descrição",
    },
    { id: 4, name: "bola vazia", valor: 400, descricao: "produto medio" },
    { id: 5, name: "bola cheia", valor: 500, descricao: "produto n sei" },
    { id: 6, name: "bola de gude", valor: 600, descricao: "produto talvez" },
    { id: 7, name: "bolita", valor: 700, descricao: "produto barato" },
    { id: 8, name: "bolado", valor: 800, descricao: "produto caro" },
];

const carrinho = [];