const swiper = new Swiper(".swiper-container", {
    autoplay: {
        delay: 5000,
    },
    // Optional parameters
    direction: "vertical",
    loop: true,
    zoom: {
        maxRatio: 5,
    },
    // If we need pagination
    pagination: {
        el: ".swiper-pagination",
    },

    // Navigation arrows
    navigation: {
        nextEl: ".swiper-button-next",
        prevEl: ".swiper-button-prev",
    },
});