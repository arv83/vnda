gerar_produtos(produtos);

function gerar_produtos(produtos) {
    const LOJA = document.getElementById("loja");
    LOJA.innerHTML = "";
    produtos.map((elem, i) => {
        const NOVADIV = document.createElement("div");
        NOVADIV.setAttribute("id", "produto");
        NOVADIV.innerHTML = "<h2>" + elem.name + "</h2>";
        NOVADIV.innerHTML += "<img src=https://picsum.photos/130/130></img>";

        NOVADIV.innerHTML += "<div>descrição: <em>" + elem.descricao + "</em><br>";
        NOVADIV.innerHTML += "Valor R$ <em>" + elem.valor + "</em></div><br>";
        NOVADIV.innerHTML +=
            "<button onclick='comprar(" + elem.id + ")''>Comprar</button>";

        LOJA.appendChild(NOVADIV);
    });
}

function comprar(x) {
    if (!carrinho.length || carrinho.findIndex((s) => s.id == x) == -1) {
        carrinho.push({
            id: x,
            qtd: 1,
        });
    } else {
        var index = carrinho.findIndex((s) => s.id == x);
        carrinho[index].qtd += 1;
    }
    let total = 0;
    carrinho.map((elem) => (total += elem.qtd));

    const AVISO = document.getElementById("aviso");
    AVISO.style.display = "block";
    AVISO.innerHTML = "<h2>" + total + "</h2>";
}

function verifica_click_fora(e) {
    for (let i = 0; i < e.path.length; i++) {
        if (e.path[i].id === "busca" || e.path[i].id === "carrinho") {
            return true;
        }
    }
    return false;
}

window.onclick = function(e) {
    try {
        for (let i = 0; i < e.path.length; i++) {
            if (e.path[i].id === "busca" || e.path[i].src.includes("lupa") === true) {
                fecha_carrinho();
                break;
            }

            if (
                e.path[i].id === "carrinho" ||
                e.path[i].src.includes("buy") === true
            ) {
                fecha_busca();
                break;
            }
        }
    } catch (a) {
        if (!verifica_click_fora(e)) {
            fecha_busca();
            fecha_carrinho();
        }
    }
};

function fecha_busca() {
    var visivel = document.getElementById("busca");
    visivel.style.display = "none";
}

function fecha_carrinho() {
    var visivel = document.getElementById("carrinho");
    visivel.innerHTML = "";
    visivel.style.display = "none";
}

function ver_carrinho() {
    var visivel = document.getElementById("carrinho");
    visivel.innerHTML = "";

    if (visivel.style.display === "none" || !visivel.style.display) {
        visivel.style.display = "block";
        let total_compra = 0;
        carrinho.map((prod, i) => {
            prod_atual = produtos.find(
                (busca_prod, index, array) => busca_prod.id === prod.id
            );

            const NOVO_PRODUTO = document.createElement("div");
            NOVO_PRODUTO.setAttribute("class", "carrinho_produto");
            NOVO_PRODUTO.innerHTML += "<h2>" + prod_atual.name + "</h2>";
            NOVO_PRODUTO.innerHTML +=
                "<h2>qtd " +
                prod.qtd +
                ", preço R$ " +
                prod_atual.valor +
                ", total " +
                prod.qtd * prod_atual.valor +
                "</h2>";
            document.getElementById("carrinho").appendChild(NOVO_PRODUTO);
            total_compra += prod.qtd * prod_atual.valor;
        });

        const NOVO_PRODUTO = document.createElement("div");
        NOVO_PRODUTO.setAttribute("class", "carrinho_produto");
        if (total_compra == 0) {
            NOVO_PRODUTO.innerHTML += "<h2>Adicione produtos!!</h2> ";
        } else {
            NOVO_PRODUTO.innerHTML += "<h2>Total da compra " + total_compra + "</h2>";
        }
        document.getElementById("carrinho").appendChild(NOVO_PRODUTO);
    } else {
        visivel.style.display = "none";
    }
}

function busca() {
    var visivel = document.getElementById("busca");
    // visivel.innerHTML = "";

    if (visivel.style.display === "none" || !visivel.style.display) {
        visivel.style.display = "block";
    } else {
        visivel.style.display = "none";
    }
}
document.getElementById("botao_busca").onclick = function(e) {
    e.preventDefault();

    var resultado = [];

    for (var i = 0; i < produtos.length; i++) {
        if (
            produtos[i].name.indexOf(document.getElementById("buscou").value) >= 0
        ) {
            resultado.push(produtos[i]);
        }
    }
    console.log(resultado);
    gerar_produtos(resultado);
};